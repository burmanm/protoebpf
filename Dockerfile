FROM fedora:26

MAINTAINER Michael Burman <yak@iki.fi>

COPY protoebpf /

ENTRYPOINT ["/protoebpf"]
