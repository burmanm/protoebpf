package main

import (
	"fmt"
	"time"

	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

func pods() {
	// creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}
	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	for {
		pods, err := clientset.CoreV1().Pods("").List(metav1.ListOptions{})
		if err != nil {
			panic(err.Error())
		}
		fmt.Printf("There are %d pods in the cluster\n", len(pods.Items))

		for _, p := range pods.Items {
			fmt.Printf("Pod name: %s -> %d labels, ip -> %s\n", p.Name, len(p.GetLabels()), p.Status.PodIP)
			// All I need is the exposed port I guess.. of the service?
		}

		ends, err := clientset.CoreV1().Endpoints("").List(metav1.ListOptions{})
		if err != nil {
			panic(err.Error())
		}

		for _, e := range ends.Items {

			fmt.Printf("Endpoint: %s ", e.GetName())
			for _, eb := range e.Subsets {
				for _, ea := range eb.Addresses {
					fmt.Printf(", ip->%s", ea.IP)
				}
				for _, ep := range eb.Ports {
					fmt.Printf(", port->%d", ep.Port)
				}
			}
			fmt.Printf("\n")

		}

		srvs, err := clientset.CoreV1().Services("").List(metav1.ListOptions{})
		if err != nil {
			panic(err.Error())
		}

		for _, s := range srvs.Items {
			fmt.Printf("\nService: %s, clusterIP->%s ", s.GetName(), s.Spec.ClusterIP)

			for _, sip := range s.Spec.ExternalIPs {
				fmt.Printf(", ip->%s", sip)
			}

			for _, sp := range s.Spec.Ports {
				fmt.Printf(", port->%d", sp.Port)
			}
		}

		fmt.Printf("\n")

		// Examples for error handling:
		// - Use helper functions like e.g. errors.IsNotFound()
		// - And/or cast to StatusError and use its properties like e.g. ErrStatus.Message
		_, err = clientset.CoreV1().Pods("default").Get("example-xxxxx", metav1.GetOptions{})
		if errors.IsNotFound(err) {
			fmt.Printf("Pod not found\n")
		} else if statusError, isStatus := err.(*errors.StatusError); isStatus {
			fmt.Printf("Error getting pod %v\n", statusError.ErrStatus.Message)
		} else if err != nil {
			panic(err.Error())
		} else {
			fmt.Printf("Found pod\n")
		}

		time.Sleep(10 * time.Second)
	}
}
