package main

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

type KubeIntegration struct {
	Client *kubernetes.Clientset
}

type Pod struct {
	Name string
}

func NewKubeIntegration() *KubeIntegration {
	// creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil {
		// TODO Error handling
		panic(err.Error())
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		// TODO Error handling
		panic(err.Error())
	}

	return &KubeIntegration{
		Client: clientset,
	}
}

// Pods returns a map of ip -> podname for Vizceral
func (k *KubeIntegration) Pods() (map[string]string, error) {
	pods, err := k.Client.CoreV1().Pods("sock-shop").List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	r := make(map[string]string, len(pods.Items))
	for _, p := range pods.Items {
		r[p.Status.PodIP] = p.Name
	}

	ends, err := k.Client.CoreV1().Endpoints("sock-shop").List(metav1.ListOptions{})
	if err != nil {
		panic(err.Error())
	}

	for _, e := range ends.Items {
		for _, eb := range e.Subsets {
			for _, ea := range eb.Addresses {
				r[ea.IP] = e.GetName()
			}
		}
	}

	srvs, err := k.Client.CoreV1().Services("sock-shop").List(metav1.ListOptions{})
	if err != nil {
		panic(err.Error())
	}

	for _, s := range srvs.Items {
		for _, sip := range s.Spec.ExternalIPs {
			r[sip] = s.GetName()
		}
	}

	return r, nil
}
