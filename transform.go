package main

import (
	"time"
)

func PodsToNodes(pods map[string]string, connections map[string][]VizceralConnection) []VizceralNode {
	nodes := make([]VizceralNode, 0, len(pods))
	for _, v := range pods {
		vn := VizceralNode{
			Name:    v,
			Updated: time.Now().UnixNano() / 1000,
		}
		if c, hit := connections[v]; hit {
			vn.Connections = c
		}
		nodes = append(nodes, vn)
	}
	return nodes
}
