package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	// viz "github.com/adrianco/go-vizceral"
)

type ProtoEbpfHandler struct {
	source  *KubeIntegration
	tracker *EbpfTracker
}

func main() {
	fmt.Printf("Starting protoebpf\n")

	pods()

	et, err := NewEbpfTracker()
	if err != nil {
		panic(err)
	}

	p := ProtoEbpfHandler{
		source:  NewKubeIntegration(),
		tracker: et,
	}
	fmt.Println(p.startServer())
}

func (h *ProtoEbpfHandler) startServer() error {
	mux := http.NewServeMux()
	mux.HandleFunc("/vizceral", h.vizceralExampleHandler)
	return http.ListenAndServe(":10090", mux)
}

func (h *ProtoEbpfHandler) vizceralExampleHandler(w http.ResponseWriter, req *http.Request) {
	pods, err := h.source.Pods()
	if err != nil {
		fmt.Println(err)
	}

	// TODO Separate the following to its own function
	connections := make(map[string][]VizceralConnection, len(pods))

	h.tracker.walkConnections(func(e ebpfConnection) {

		if srcpod, found := pods[e.tuple.fromAddr]; found {
			if destpod, f := pods[e.tuple.toAddr]; f {
				//				if srcpod != destpod {
				if _, exists := connections[srcpod]; !exists {
					connections[srcpod] = make([]VizceralConnection, 1)
				}
				connections[srcpod] = append(connections[srcpod], VizceralConnection{
					Source: srcpod,
					Target: destpod,
					Class:  "normal",
				})
				//				}
				fmt.Printf("Connection from pod->%s to pod->%s\n", srcpod, destpod)
			} else {
				fmt.Printf("No destPod for IP %s found\n", e.tuple.toAddr)
			}
		} else {
			fmt.Printf("No pod for IP %s found\n", e.tuple.fromAddr)
		}
		fmt.Printf("Connection from ip->%s to ip->%s\n", e.tuple.fromAddr, e.tuple.toAddr)
	})

	nodes := PodsToNodes(pods, connections)

	v := VizceralGraph{
		Renderer: "global",
		Name:     "INTERNET",
		Nodes:    nodes,
	}

	b, err := json.Marshal(v)
	if err != nil {
		fmt.Println(err)
	}

	_, err = w.Write(b)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("Served data with %d pods!\n", len(pods))

	/*
		Process: Link pods to network connections
		Create VizceralNodes from the pods
	*/
}
